package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {

    /**
     * Fields created by group 9.
     */
    ArrayList<Dosis> dosisArrayList = new ArrayList<>();

    /**
     * Constructorfor objects of type "DagligSkaev" (daily ordinations at set times).
     *
     * @param startDen Date of first administration of drug.
     * @param slutDen Date of last administration of drug.
     * @param laegemiddel the drug to be administered.

     */
    public DagligSkaev(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel){
        super(startDen, slutDen, laegemiddel);
    }

    /**
     * Create dose of medicine at given time and amount.
     *
     * @param tid Time at which dose must be administered.
     * @param antal Amount of doses to administer.
     */
    public boolean opretDosis(LocalTime tid, double antal) throws IllegalArgumentException {

        boolean dosisExists = false;

            for (Dosis dosis : dosisArrayList) {
                if (tid == dosis.getTid()) {
                    dosisExists = true;
                }
            }

            if (!dosisExists) {

                    if (antal > 0) {
                    Dosis dosis = new Dosis(tid, antal);
                    dosisArrayList.add(dosis);
                }

            }

        return dosisExists;
    }

    /**
     * Method deletes a specific dose from patient's dosis array.
     *
     * @param dosis is the dose to be deleted.
     */
    public void deleteDose(Dosis dosis){

        if (dosisArrayList.contains(dosis)){
            dosisArrayList.remove(dosis);
        }
    }

    public ArrayList<Dosis> getDosisArrayList() {
        return dosisArrayList;
    }

    @Override
    public String toString() {
        return getStartDen().toString();
    }

    /**
     * Returnerer den totale dosis der er givet i den periode ordinationen er gyldig
     * @return
     */
    public double samletDosis(){

        double totalDosis = doegnDosis() * antalDage();

        return totalDosis;
    }

    /**
     * Returnerer den gennemsnitlige dosis givet pr dag i den periode ordinationen er gyldig
     * @return
     */
    public double doegnDosis(){

        double totalDoegnDosis = 0;

        for (Dosis dosis : dosisArrayList){
            totalDoegnDosis += dosis.getAntal();
        }

        return totalDoegnDosis;
    }

    /**
     * Returnerer ordinationstypen som en String
     * @return
     */
    public String getType(){
        return "Skaev";
    }
}

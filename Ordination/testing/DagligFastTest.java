package testing;

import ordination.DagligFast;
import ordination.Laegemiddel;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.junit.Assert.*;

/**
 * Created by Kristobal on 18-09-2017.
 */
public class DagligFastTest {

    Laegemiddel panodil = new Laegemiddel("Paracetamol", 1.0, 1.5,
                                          2.0, "ml");
    LocalDate startDato = LocalDate.of(2001, 01, 01);
    LocalDate slutDato =LocalDate.of(2001, 01, 10);

    DagligFast dagligFast0 = new DagligFast(startDato, slutDato, panodil);

    @Test
    public void samletDosis() throws Exception {

        dagligFast0.createDoses(1,2, 3, 4);
        assertEquals(100, dagligFast0.samletDosis(), 0.000001);

    }

    @Test
    public void doegnDosis() throws Exception {

        dagligFast0.createDoses(1,2, 3, 4);
        assertEquals(10, dagligFast0.samletDosis(), 0.000001);

    }

}
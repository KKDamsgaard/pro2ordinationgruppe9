package ordination;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class PN extends Ordination{

    private double antalEnheder;
    private int antalGangeGivet = 0;
    private ArrayList<LocalDateTime> doseDatoer = new ArrayList<>();

    /**
     * Constructor for objects of type "PN" (Pro Necesare, medicine given as necessary).
     *
     * @param startDen Date of beginning of period.
     * @param slutDen Date of end of period.
     * @param laegemiddel the drug to be given
     * @param antalEnheder amount units to be given each time.
     */
    public PN(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, double antalEnheder) {
        super(startDen, slutDen, laegemiddel);
        this.antalEnheder = antalEnheder;
    }

    /**
     * Registrerer at der er givet en dosis paa dagen givesDen
     * Returnerer true hvis givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
     * Retrurner false ellers og datoen givesDen ignoreres
     * @param givesDen
     * @return
     */
    public boolean givDosis(LocalDate givesDen) {

        if (givesDen.isAfter(getStartDen().minusDays(1)) && givesDen.isBefore(getSlutDen().plusDays(1))){

            LocalDateTime timeStamp = givesDen.atTime(LocalTime.now());
            doseDatoer.add(timeStamp);
            antalGangeGivet++;
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Returnerer antal gange ordinationen er anvendt
     * @return
     */
    public int getAntalGangeGivet() {
        return antalGangeGivet;
    }

    public double getAntalEnheder() {
        return antalEnheder;
    }

    public ArrayList<LocalDateTime> getDoseDatoer() {
        return doseDatoer;
    }

    /**
     * Returnerer den gennemsnitlige dosis givet pr dag i den periode ordinationen er gyldig
     * @return returns total amount of doses of medicine given for the specified date.
     */
    public double doegnDosis() {
        int days = (int) (ChronoUnit.DAYS.between(getStartDen(), getSlutDen()) + 1);

        if (antalGangeGivet * antalEnheder == 0){
            return 0;
        }
        else {
            return antalGangeGivet * antalEnheder / days;
        }
    }

    /**
     * Returnerer den totale dosis der er givet i den periode ordinationen er gyldig
     * @return
     */
    public double samletDosis() {
        return antalGangeGivet * antalEnheder;
    }

    /**
     * Returnerer ordinationstypen som en String
     * @return
     */
    public String getType(){
        return "Pro Necesare";
    }

}

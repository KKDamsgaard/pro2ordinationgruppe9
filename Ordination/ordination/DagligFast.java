package ordination;


import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class DagligFast extends Ordination {

    private Dosis[] doser = new Dosis[4];
    private Laegemiddel laegemiddel;

    public DagligFast(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel) {
        super(startDen, slutDen, laegemiddel);
    }

    public Dosis[] getDoser() {
        return doser;
    }

    /**
     * Sets doses for the PN object
     *
     * @param morgenAntal is amount of doses to be given in the morning.
     * @param middagAntal is amount of doses to be given at noon.
     * @param aftenAntal is amount of doses to be given in the evening.
     * @param natAntal is amount of doses to be given at night.
     */
    public void createDoses(double morgenAntal, double middagAntal, double aftenAntal, double natAntal){

        doser[0] = new Dosis(LocalTime.of(6, 0), morgenAntal);
        doser[1] = new Dosis(LocalTime.of(12, 0), middagAntal);
        doser[2] = new Dosis(LocalTime.of(18, 0), aftenAntal);
        doser[3] = new Dosis(LocalTime.of(23, 59), natAntal);
    }

    @Override
    public String toString() {
        return getStartDen().toString();
    }

    /**
     * Returnerer den totale dosis der er givet i den periode ordinationen er gyldig
     * @return
     */
    public double samletDosis(){

        double totalDosis = 0;
        double dagsDosis = 0;

        for(Dosis dose : doser){
            dagsDosis += dose.getAntal();
        }

        totalDosis = dagsDosis * antalDage();

        return totalDosis;
    }

    /**
     * Returnerer den gennemsnitlige dosis givet pr dag i den periode ordinationen er gyldig
     * @return
     */
    public double doegnDosis(){

        double totalDoegnDosis = 0;

        for (Dosis dosis : doser){
            totalDoegnDosis += dosis.getAntal();
        }

        totalDoegnDosis = totalDoegnDosis / (ChronoUnit.DAYS.between(getStartDen(), getSlutDen()) + 1);

        return totalDoegnDosis;
    }

    /**
     * Returnerer ordinationstypen som en String
     * @return
     */
    public String getType(){
        return "Fast";
    }
}

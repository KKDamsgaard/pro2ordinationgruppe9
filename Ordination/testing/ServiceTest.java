package testing;

import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;
import org.junit.Test;
import service.Service;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.junit.Assert.*;

/**
 * Created by Kristobal on 18-09-2017.
 */
public class ServiceTest {

    Laegemiddel panodil = new Laegemiddel("Paracetamol", 1.0, 1.5,
            2.0, "ml");
    Patient patientAverage = new Patient("012345-6789", "ABCDEFG", 100);
    Patient patientLight = new Patient("123450-7896", "HIJKLMN", 10);
    Patient patientHeavy = new Patient("234501-8967", "OPQRSTU", 1000);
    LocalDate startDato = LocalDate.of(2001, 01, 01);
    LocalDate slutDato = LocalDate.of(2001, 01, 10);

    @Test (expected = IllegalArgumentException.class)
    public void opretPNOrdination_slutBeforeStart() throws Exception {

        Service.getService().opretPNOrdination(slutDato, startDato, patientAverage, panodil, 1);

    }

    @Test (expected = IllegalArgumentException.class)
    public void opretPNOrdination_unitLessThanOne() throws Exception {

        Service.getService().opretPNOrdination(startDato, slutDato, patientAverage, panodil, 0);

    }

    @Test
    public void opretPNOrdination_isWorking() throws Exception {

        assertNotNull(Service.getService().opretPNOrdination(startDato, slutDato, patientAverage, panodil, 1));

    }

    @Test  (expected = IllegalArgumentException.class)
    public void opretDagligFastOrdination_slutBeforeStart() throws Exception {

        Service.getService().opretDagligFastOrdination(slutDato, startDato, patientAverage, panodil, 1,
                                                        2, 3, 4);

    }

    @Test  (expected = IllegalArgumentException.class)
    public void opretDagligFastOrdination_antalLessZero() throws Exception {

        Service.getService().opretDagligFastOrdination( startDato, slutDato, patientAverage, panodil, 1,
                2, -1, 4);

    }

    @Test
    public void opretDagligFastOrdination_isWorking() throws Exception {

        Service.getService().opretDagligFastOrdination(startDato, slutDato, patientAverage, panodil, 1,
                2, 3, 4);

    }

    @Test (expected = IllegalArgumentException.class)
    public void opretDagligSkaevOrdination_slutBeforeStart() throws Exception {

        LocalTime[] times = new LocalTime[1];
        double[] units = new double[1];

        times[0] = LocalTime.of(12, 00);
        units[0] = 1;


        Service.getService().opretDagligSkaevOrdination(slutDato, startDato, patientAverage, panodil, times, units);

    }

    @Test (expected = IllegalArgumentException.class)
    public void opretDagligSkaevOrdination_antalLessEqualZero() throws Exception {

        LocalTime[] times = new LocalTime[1];
        double[] units = new double[1];

        times[0] = LocalTime.of(12, 00);
        units[0] = -1;

        Service.getService().opretDagligSkaevOrdination(startDato, slutDato, patientAverage, panodil, times, units);
    }

    @Test (expected = IllegalArgumentException.class)
    public void opretDagligSkaevOrdination_timeAlreadyExists() throws Exception {

        LocalTime[] times = new LocalTime[2];
        double[] units = new double[2];

        times[0] = LocalTime.of(12, 00);
        times[1] = LocalTime.of(12, 00);
        units[0] = 1;
        units[1] = 1;

        Service.getService().opretDagligSkaevOrdination(startDato, slutDato, patientAverage, panodil, times, units);
    }

    @Test
    public void opretDagligSkaevOrdination_isWorking() throws Exception {

        LocalTime[] times = new LocalTime[2];
        double[] units = new double[2];

        times[0] = LocalTime.of(12, 00);
        times[0] = LocalTime.of(16, 00);
        units[0] = 1;
        units[1] = 1;

        Service.getService().opretDagligSkaevOrdination(startDato, slutDato, patientAverage, panodil, times, units);
    }

    @Test (expected = IllegalArgumentException.class)
    public void ordinationPNAnvendt_dateBeforeStart() throws Exception {

        PN pn0 = Service.getService().opretPNOrdination(startDato, slutDato, patientAverage, panodil, 1);

        LocalDate date0 = LocalDate.of(2000, 12, 31);

        Service.getService().ordinationPNAnvendt(pn0, date0);

    }

    @Test (expected = IllegalArgumentException.class)
    public void ordinationPNAnvendt_dateAfterSlut() throws Exception {

        PN pn0 = Service.getService().opretPNOrdination(startDato, slutDato, patientAverage, panodil, 1);

        LocalDate date0 = LocalDate.of(2001, 01, 11);

        Service.getService().ordinationPNAnvendt(pn0, date0);
    }

    @Test
    public void ordinationPNAnvendt_isWorking() throws Exception {

        PN pn0 = Service.getService().opretPNOrdination(startDato, slutDato, patientAverage, panodil, 1);

        LocalDate date0 = LocalDate.of(2001, 01, 01);

        Service.getService().ordinationPNAnvendt(pn0, date0);

        assertEquals(date0, pn0.getDoseDatoer().get(0).toLocalDate());
    }

    @Test
    public void anbefaletDosisPrDoegn_average() throws Exception {

        assertEquals(150, Service.getService().anbefaletDosisPrDoegn(patientAverage, panodil), 0.1);

    }

    @Test
    public void anbefaletDosisPrDoegn_light() throws Exception {

        assertEquals(10, Service.getService().anbefaletDosisPrDoegn(patientLight, panodil), 0.1);

    }

    @Test
    public void anbefaletDosisPrDoegn_heavy() throws Exception {

        assertEquals(2000, Service.getService().anbefaletDosisPrDoegn(patientHeavy, panodil), 0.1);

    }

    @Test
    public void antalOrdinationerPrVægtPrLægemiddel_light() throws Exception {

        //TODO test not working, logic error in Service method?

        Service.getService().opretDagligFastOrdination(startDato, slutDato, patientLight, panodil,
                                                        1, 1, 1, 1);
        Service.getService().opretDagligFastOrdination(startDato, slutDato, patientAverage, panodil,
                1, 1, 1, 1);
        Service.getService().opretDagligFastOrdination(startDato, slutDato, patientHeavy, panodil,
                1, 1, 1, 1);

        assertEquals(1,
                Service.getService().antalOrdinationerPrVægtPrLægemiddel(1, 11, panodil), 0.1);

        assertEquals(2,
                Service.getService().antalOrdinationerPrVægtPrLægemiddel(1, 101, panodil), 0.1);
        assertEquals(3,
                Service.getService().antalOrdinationerPrVægtPrLægemiddel(1, 1001, panodil), 0.1);
    }

}
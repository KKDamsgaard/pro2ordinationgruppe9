package ordination;

import java.util.ArrayList;

public class Patient {
    private String cprnr;
    private String navn;
    private double vaegt;

    /**
     * Field created by group 9.
     */
    ArrayList<Ordination> ordinations = new ArrayList<>();

    public Patient(String cprnr, String navn, double vaegt) {
        this.cprnr = cprnr;
        this.navn = navn;
        this.vaegt = vaegt;
    }

    public String getCprnr() {
        return cprnr;
    }

    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
        this.navn = navn;
    }

    public double getVaegt(){
        return vaegt;
    }

    public void setVaegt(double vaegt){
        this.vaegt = vaegt;
    }

    /**
     * Returns Array of ordinations.
     *
     * @return ArrayList containing ordinations.
     */
    public ArrayList<Ordination> getOrdinations() {
        return ordinations;
    }

    /**
     * Adds an ordination to the list of ordinations
     *
     * @param o ordination to add to list.
     */
    public void addOrdination(Ordination o){
        if (!ordinations.contains(o)) {
            ordinations.add(o);
        }
    }

    /**
     * Removes an ordination from the list of ordinations.
     *
     * @param o ordination to remove.
     */
    public void removeOrdination(Ordination o){
        if (ordinations.contains(0)){
            ordinations.remove(o);
        }
    }

    @Override
    public String toString(){
        return navn + "  " + cprnr;
    }

}

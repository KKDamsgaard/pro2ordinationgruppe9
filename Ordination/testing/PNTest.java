package testing;

import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.*;

/**
 * Created by Kristobal on 18-09-2017.
 */
public class PNTest {

    Laegemiddel panodil = new Laegemiddel("Paracetamol", 1.0, 1.5,
            2.0, "ml");
    LocalDate startDato = LocalDate.of(2001, 01, 01);
    LocalDate slutDato = LocalDate.of(2001, 01, 10);

    @Test
    public void givDosis_givesBeforeStart() throws Exception {

        PN proNecesare0 = new PN(startDato, slutDato, panodil, 1);

        assertFalse(proNecesare0.givDosis(LocalDate.of(2001, 12, 31)));

    }

    @Test
    public void givDosis_givesAfterSlut() throws Exception {

        PN proNecesare0 = new PN(startDato, slutDato, panodil, 1);

        assertFalse(proNecesare0.givDosis(LocalDate.of(2001, 01, 11)));

    }

    @Test
    public void givDosis_virker() throws Exception {

        PN proNecesare0 = new PN(startDato, slutDato, panodil, 1);

        assertEquals(0, proNecesare0.getDoseDatoer().size());

        proNecesare0.givDosis(LocalDate.of(2001, 01, 01));

        assertEquals(1, proNecesare0.getDoseDatoer().size());

    }

    @Test
    public void doegnDosis() throws Exception {

        PN proNecesare0 = new PN(startDato, slutDato, panodil, 1);

        assertEquals(0, proNecesare0.doegnDosis(), 0.1);

        proNecesare0.givDosis(LocalDate.of(2001, 01, 01));

        assertEquals(0.1, proNecesare0.doegnDosis(),0.0001);

    }

    @Test
    public void samletDosis() throws Exception {

        PN proNecesare0 = new PN(startDato, slutDato, panodil, 1);

        assertEquals(0, proNecesare0.doegnDosis(), 0.1);

        proNecesare0.givDosis(LocalDate.of(2001, 01, 01));

        assertEquals(1, proNecesare0.samletDosis(), 0.0001);

    }

}
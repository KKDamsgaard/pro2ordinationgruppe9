package testing;

import ordination.DagligSkaev;
import ordination.Laegemiddel;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.junit.Assert.*;

/**
 * Created by Kristobal on 18-09-2017.
 */
public class DagligSkaevTest {

    Laegemiddel panodil = new Laegemiddel("Paracetamol", 1.0, 1.5,
            2.0, "ml");
    LocalDate startDato = LocalDate.of(2001, 01, 01);
    LocalDate slutDato = LocalDate.of(2001, 01, 10);


    @Test
    public void opretDosis_isWorking() throws Exception {

        DagligSkaev dagligSkaev0 = new DagligSkaev(startDato, slutDato, panodil);

        dagligSkaev0.opretDosis(LocalTime.of(12, 00), 1);
        assertNotNull(dagligSkaev0.getDosisArrayList().get(0));
    }

    @Test
    public void dublicateTime() throws Exception {

        DagligSkaev dagligSkaev0 = new DagligSkaev(startDato, slutDato, panodil);

        dagligSkaev0.opretDosis(LocalTime.of(12, 00), 1);

        assertTrue(dagligSkaev0.opretDosis(LocalTime.of(12, 00), 1));
    }

    @Test
    public void opretDosis_doseZero() throws Exception {

        DagligSkaev dagligSkaev0 = new DagligSkaev(startDato, slutDato, panodil);

        assertNotNull(dagligSkaev0.opretDosis(LocalTime.of(10, 00), 0));

    }

    @Test
    public void samletDosis() throws Exception {

        DagligSkaev dagligSkaev0 = new DagligSkaev(startDato, slutDato, panodil);

        dagligSkaev0.opretDosis(LocalTime.of(12, 00), 1);
        assertEquals(10, dagligSkaev0.samletDosis(), 0.0001);

    }

    @Test
    public void doegnDosis() throws Exception {

        DagligSkaev dagligSkaev0 = new DagligSkaev(startDato, slutDato, panodil);

        dagligSkaev0.opretDosis(LocalTime.of(12, 00), 1);
        assertEquals(1, dagligSkaev0.doegnDosis(), 0.0001);

    }

}